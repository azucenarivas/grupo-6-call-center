create database if not exists grupo6_callcenter;
use grupo6_callcenter;

create table if not exists personal(
id_personal int not null primary key auto_increment,
nombres varchar(100) not null,
apellidos varchar(100) not null,
dui varchar(11) not null,
nit varchar(17) not null,
telefono varchar(9) not null,
direccion varchar(150) not null,
email varchar(60) not null,
fecha_nacimiento date not null
)engine InnoDB;

create table if not exists cargos(
id_cargo int not null primary key auto_increment,
cargo varchar(50) not null
)engine InnoDB;

create table if not exists usuarios(
id_personal int not null primary key,
usuario varchar(15) not null,
pass varchar(10) not null,
id_cargo int not null,
constraint fk_personal_usuarios foreign key (id_personal) references personal(id_personal) on delete cascade on update cascade,
constraint fk_cargos_usuarios foreign key (id_cargo) references cargos(id_cargo) on delete cascade on update cascade
)engine InnoDB;

create table if not exists turnos(
id_turno int not null primary key auto_increment,
fecha_inicio date not null,
fecha_fin date not null,
hora_inicio time not null,
hora_fin time not null,
id_personal int not null,
constraint fk_empleado_turnos foreign key (id_personal) references personal(id_personal) on delete cascade on update cascade
)engine InnoDB;

create table if not exists registro_turnos(
id_registro int not null primary key auto_increment,
id_turno int not null,
constraint fk_turnos_registro_turnos foreign key (id_turno) references turnos(id_turno) on delete cascade on update cascade
)engine InnoDB;


create table if not exists salas(
id_sala int not null primary key auto_increment,
nombre varchar(100) not null,
tipo varchar(100) not null,
id_registro int not null,
constraint fk_registro_turnos_salas foreign key (id_registro) references registro_turnos(id_registro) on delete cascade on update cascade
)engine InnoDB;

create table if not exists areas(
id_area int not null primary key auto_increment,
nombre varchar(100) not null,
id_sala int not null,
constraint fk_salas_areas foreign key (id_sala) references salas(id_sala) on delete cascade on update cascade
)engine InnoDB;

create table if not exists metas(
id_meta int not null primary key auto_increment,
fecha date not null,
hora time not null,
descripcion varchar(100) not null,
id_personal int not null,
constraint fk_personal_metas foreign key (id_personal) references personal(id_personal) on delete cascade on update cascade
)engine InnoDB;

create table if not exists sanciones(
id_sancion int not null primary key auto_increment,
tipo varchar(50) not null,
fecha date not null,
hora time not null,
descripcion varchar(100) not null,
descuento double
)engine InnoDB;

create table if not exists bonificaciones(
id_bonificacion int not null primary key auto_increment,
tipo varchar(50) not null,
fecha date not null,
hora time not null,
descripcion varchar(100) not null,
bono double
)engine InnoDB;

create table if not exists entrenamientos(
id_entrenamiento int not null primary key auto_increment,
tipo varchar(50) not null,
descripcion varchar(100) not null,
id_registro int not null,
constraint fk_registro_turnos_entrenamientos foreign key (id_registro) references registro_turnos(id_registro) on delete cascade on update cascade
)engine InnoDB;

create table if not exists subsalarios(
id_subsalario int not null primary key auto_increment,
pago double not null,
id_sancion int not null,
id_bonificacion int not null,
id_personal int not null,
constraint fk_sanciones_subsalarios foreign key (id_sancion) references sanciones(id_sancion) on delete cascade on update cascade,
constraint fk_bonificaciones_subsalarios foreign key (id_bonificacion) references bonificaciones(id_bonificacion) on delete cascade on update cascade,
constraint fk_personal_subsalarios foreign key (id_personal) references personal(id_personal) on delete cascade on update cascade
)engine InnoDB;

create table if not exists pagos(
id_pago int not null primary key auto_increment,
total_pagar double not null,
id_subsalario int not null,
constraint fk_subsalarios_pagos foreign key (id_subsalario) references subsalarios(id_subsalario) on delete cascade on update cascade
)engine InnoDB;

create table if not exists solicitudes(
id_solicitud int not null primary key auto_increment,
motivo varchar(50) not null,
fecha_inicio date not null,
fecha_fin date not null,
id_personal int not null,
constraint fk_empleado_solicitudes foreign key (id_personal) references personal(id_personal) on delete cascade on update cascade
)engine InnoDB;


